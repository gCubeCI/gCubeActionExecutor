# gCubeActionExecutor

Starting from a list of git projects and a Bash fragment, for each project, this Jenkins pipeline:
 * clone the git repository
 * execs the Bash fragment in the root folder of the repository
 * pushes back the repository using `git.gcube` as author.

## Requirements
* [Jenkins](https://jenkins.io/) ver. 2.164.2 or newer
* [Pipeline plugin](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin) ver. 2.5 or newer
* [Pipeline: SCM Step plugin](https://plugins.jenkins.io/workflow-scm-step) ver. 2.7 or newer
* [Pipeline: Shared Groovy Libraries](https://plugins.jenkins.io/workflow-cps-global-lib) ver. 2.15 or newer
* User credentials for git.gcube configured on Jenkins. These are needed to set the author of the changes in the repository.

## Parameters
* _Git Root_: The URL of the parent organization that includes all the listed repositories.
* _List of Repositories_: A plain text file, one line for each Git repository name to update.
* _Action Root_: The root URL of the Bash fragment to execute.
* _Action File_: The relative path under the Action Root of a Bash script 

## How Does It Work
The pipeline:
1. clones one repository at the time from _Git Root_/_Repo Name_ (from the list)
2. downloads the action file from _Action Root_/_Action File_ in the root folder of the repository
3. runs the action file using the root folder of the repository as working dir
4. pushes the repository back to Gitea

Every change to the Git repository **MUST** be done by the action file. The pipeline is in charge only
to push the repository to the remote origin.

## Sample Usage

### List of Repositories
````text
52n-wps-algorithm-gcube
52n-wps-server-gcube 
about-vre 
accept-invite-portlet 
accounting-aggregator-se-plugin 
accounting-analytics 
...
````

### Action File
The following fragment downloads a file named LICENSE.md and adds it to the repository (if it does not exist):

````bash
#!/usr/bin/env bash
if [[ ! -f LICENSE.md ]]; then
    curl <my URL>/LICENSE.md -o LICENSE.md
    git add LICENSE.md
    git commit -m "Add LICENSE.md."
fi
````  


## References
* [Pipeline as code](https://jenkins.io/doc/book/pipeline-as-code/)
* [Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/)
* [Shared Libraries](https://jenkins.io/doc/book/pipeline/shared-libraries/)
* [SCM Step](https://jenkins.io/doc/pipeline/steps/workflow-scm-step/)
* [Workflow Durable Task Step](https://www.jenkins.io/doc/pipeline/steps/workflow-durable-task-step/)
## Wiki doc
* [CI: gCube Actions Pipeline](https://wiki.gcube-system.org/gcube/Continuous_Integration:_Actions_Jenkins_Pipeline)

## License
This project is licensed under the [EUPL V.1.1 License](LICENSE.md).